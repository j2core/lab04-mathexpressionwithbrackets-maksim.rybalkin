package com.j2core.mrybalkin.week03.console_math_calculator;

import java.util.Scanner;

/**
 * Created by mrybalkin on 5/27/16.
 *
 * 1. Calculates math expression,  for example (3/4*5+(6-7)/2)-1*4/1.5 and write result (=1.25)
 *
 */
public class Calculator {

    public static void main(String[] args) throws Exception {
        System.out.println("Please enter any math expression, for example (3/4*5+(6-7)/2)-1*4/1.5");

        String str = new Scanner(System.in).nextLine();

        System.out.println("You entered: " + str);
        System.out.println("After parsing: " + Parser.parseInputString(str));
        System.out.println(MathSolver.mathSolver(Parser.parseInputString(str)));
    }
}