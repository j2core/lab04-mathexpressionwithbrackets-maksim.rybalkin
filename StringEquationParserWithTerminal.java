package com.j2core.mrybalkin.week03;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by mrybalkin on 5/27/16.
 *
 * Code task 1 week 3
 * 1. Parse simple math expression without brackets, for example 3/4*5+6-7/2-1*4/1.5 and write result
 * 2. Add input from terminal, check-in, create review
 *
 * //TODO: split this huge class for small and readable classes after adding handling brackets
 */
public class StringEquationParserWithTerminal {

    public static final char DIVISION = '/';
    public static final char MULTIPLICATION = '*';
    public static final char PLUS = '+';
    public static final char MINUS = '-';

    public static void main(String[] args) {
        System.out.println("Please enter math expression without brackets, for example 3/4*5+6-7/2-1*4/1.5");
        String str = new Scanner(System.in).nextLine();
        System.out.println("You entered: " + str);

        mathSolver(str);
    }

    /**
     * 1. Validate input string.
     * 2. Parse string to double and math sights lists
     * 3. Calculate high priority math operations (* and /),
     * remove used numbers and signs from lists.
     * 4. calculate rest of operations (+ and -)
     * 5. Print result
     *
     * @param inputString       String inputted from terminal and verified for errors
     */
    private static void mathSolver(String inputString){
        double tmp;

        String newStr = validateInputString(inputString);

        System.out.println("So, after validation your expression is:" + newStr);
        ArrayList<Double> listOfNumbers = parseToNumber(newStr);
        ArrayList<Character> listOfMathSigns = parseToMathSign(newStr);

        for (int i = 0; i < listOfMathSigns.size(); i++) {
            if (listOfMathSigns.get(i) == MULTIPLICATION || listOfMathSigns.get(i) == DIVISION) {
                tmp = calcTwoNumbers(listOfNumbers.get(i), listOfNumbers.get(i + 1), listOfMathSigns.get(i));

                listOfNumbers.set(i, tmp);
                listOfNumbers.remove(i + 1);
                listOfMathSigns.remove(i);
                i--;
            }
        }

        for (int i = 0; i < listOfMathSigns.size(); i++) {
            if (listOfMathSigns.get(i) == PLUS || listOfMathSigns.get(i) == MINUS) {
                tmp = calcTwoNumbers(listOfNumbers.get(i), listOfNumbers.get(i + 1), listOfMathSigns.get(i));

                listOfNumbers.set(i, tmp);
                listOfNumbers.remove(i + 1);
                listOfMathSigns.remove(i);
                i--;
            }
        }

        System.out.println("And the result is:" + listOfNumbers.get(0));
    }

    /**
     * 1. Replace "+-" and "-+" with "-"
     * 2. Verify input string contains alphabetic chars, If yes - exit program
     * 3. Verify input string contains double math signs, e.g. "**" or "..". If yes - replace with one sign
     *
     * E.g. user input string "1..2+-3**4++5//6", validation will return "1.2-3*4+5/6"
     *
     * @param str - input string
     * @return validated string
     */
    private static String validateInputString(String str){
        StringBuilder sb = new StringBuilder();
        String tmp = replacePlusMinus(str);

        for (int i = 0; i < tmp.length(); i++) {
            if (Character.isAlphabetic(tmp.charAt(i))) {
                System.out.println("Expression couldn't be solved, because it contains Alphabetic chars. Please verify for errors and enter again.");
                System.exit(1);
            } else if (tmp.charAt(i) == PLUS || tmp.charAt(i) == MULTIPLICATION ||
                    tmp.charAt(i) == DIVISION || tmp.charAt(i) == '.') {
                if (isCharacter(tmp.charAt(i + 1)) && tmp.charAt(i) != tmp.charAt(i + 1)) {
                    System.out.println("Entered expression is invalid. Multitype math sign is entered. Please verify for errors and enter again.");
                    System.exit(1);
                }
                while (i < tmp.length() && (tmp.charAt(i) == tmp.charAt(i + 1))) {
                    i++;
                }
                sb.append(tmp.charAt(i));

            } else {
                sb.append(tmp.charAt(i));
            }
        }

        return sb.toString();
    }

    /**
     * Converts string to array of chars,
     * parse element to double,
     *
     * @param str - input string
     * @return list of numbers
     */
    private static ArrayList<Double> parseToNumber(String str) {
        ArrayList<Double> listOfParsedNumbers = new ArrayList<Double>();

        for (int i = 0; i < str.length(); i++) {
            int j = i;

            if (str.charAt(i) == MINUS && Character.isDigit(str.charAt(i+1)) && i == 0){
                i++;
            }

            while (i < str.length() && (Character.isDigit(str.charAt(i)) || str.charAt(i) == '.')){
                i++;
            }

            listOfParsedNumbers.add(Double.parseDouble(str.substring(j, i)));
        }

        return listOfParsedNumbers;
    }

    /**
     * Converts string to array of chars,
     * if array element is math sign
     * adds it to collection
     *
     * @param str - input string
     * @return list of math signs
     */
    private static ArrayList<Character> parseToMathSign(String str){
        ArrayList<Character> listOfMathOperators = new ArrayList<Character>();
        char[] b = str.toCharArray();

        for (int i = 0; i < b.length; i++){
            if (str.charAt(i) == MINUS && i == 0) {
                i++;
            }
            if (isCharacter(str.charAt(i))) {
                listOfMathOperators.add(str.charAt(i));
            }
        }

        return listOfMathOperators;
    }

    /**
     * Calculates simple math expression
     */
    private static double calcTwoNumbers(double oper1, double oper2, Character sign){
        if (sign == MULTIPLICATION) {
           return oper1 * oper2;
        } else if (sign == DIVISION){
            if (oper2 == 0){
                System.out.println("Can't calc the result, reason - division by zero");
                System.exit(1);
            } else {
                return oper1 / oper2;
            }
        }else if (sign == PLUS) {
            return oper1 + oper2;
        }else if (sign == MINUS){
            return oper1 - oper2;
        }

        return Double.NaN;
    }

    /**
     * Validates input string.
     * If string contains spaces trim it
     * If string contains "+-" and "-+" replace it with "-"
     *
     * @param str - input string
     * @return updated string
     */
    private static String replacePlusMinus(String str){
        return str.replaceAll(" ", "").replaceAll("([+]-|-[+])", "-");
    }

    static boolean isCharacter(char c) {
        return c == PLUS || c == MINUS || c == MULTIPLICATION || c == DIVISION;
    }
}