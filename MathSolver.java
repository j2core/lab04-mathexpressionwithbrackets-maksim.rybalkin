package com.j2core.mrybalkin.week03.console_math_calculator;

import java.util.ArrayList;

/**
 * Created by mrybalkin on 6/15/16.
 */
public class MathSolver {
    /**
     * 1. Looking for open/close brackets inside input string
     * 2. If there are - cut off it and calcWithoutBrackets w/o brackets
     * 3. Recursively go through string
     *
     * @param inputString       String inputted from terminal and verified for errors
     */
    static String mathSolver(String inputString) throws Exception {
        Double interimResult;
        String res;
        int lastIndexOfCloseBracket = inputString.lastIndexOf("(");
        int firstIndexOfOpenBracket = findFirstCloseBracket(lastIndexOfCloseBracket, inputString);

        if(firstIndexOfOpenBracket>0) {
            res = inputString.substring(lastIndexOfCloseBracket + 1, firstIndexOfOpenBracket);
        } else {
            res = inputString;
        }

        res = Parser.parseInputString(res); // to support case when intermediate result is "-" (e.g. 6-7=-1)
        interimResult = calcInsideBrackets(res);

        if (firstIndexOfOpenBracket>0){
            inputString = inputString.substring(0, lastIndexOfCloseBracket) + interimResult + inputString.substring(firstIndexOfOpenBracket + 1, inputString.length());

                if (inputString.indexOf("(") > 0 || inputString.indexOf(")") > 0){
                    inputString = mathSolver(inputString);
                } else {
                    inputString = Double.toString(calcInsideBrackets(inputString));
                }
        } else {
            return interimResult.toString();
        }
        return inputString;
    }

    /**
     * 1. Split input string to numbers and math characters with respect to "minus"
     * 2. Call method for calculating
     *
     * @param inputString - input string w/o brackets
     * @return result of math operations
     * @throws Exception
     */
    private static double calcInsideBrackets(String inputString) throws Exception {
        ArrayList<Double> listOfNumbers = new ArrayList<Double>();
        ArrayList<Character> listOfMathOperators = new ArrayList<Character>();

        for (int i = 0; i < inputString.length(); i++){
            if (Character.isDigit(inputString.charAt(i)) || (inputString.charAt(i) == Parser.MINUS && (i == 0 || inputString.charAt(i - 1) == '('))) {
                int j = i;

                if ((inputString.charAt(i) == Parser.MINUS && Character.isDigit(inputString.charAt(i + 1)) && i == 0)) {
                    j++;
                } else if ((inputString.charAt(i) == Parser.MINUS && Character.isDigit(inputString.charAt(i + 1)) && inputString.charAt(i - 1) == '(')) {
                    j++;
                }

                while (j < inputString.length() && (Character.isDigit(inputString.charAt(j)) || inputString.charAt(j) == '.')) {
                    j++;
                }

                listOfNumbers.add(Double.parseDouble(inputString.substring(i, j)));
                i = j - 1;

            } else if (Parser.isCharacter(inputString.charAt(i))) {
                listOfMathOperators.add(inputString.charAt(i));
            }
        }
        return calcWithoutBrackets(listOfNumbers, listOfMathOperators);
    }

    /**
     * 1. Go through lists and perform calculation
     * 2. After numbers and math character is used remove it from the list and result add
     *
     * @param listOfNumbers
     * @param listOfMathOperators
     * @return - result of calculation
     * @throws Exception
     */
    private static double calcWithoutBrackets(ArrayList<Double> listOfNumbers, ArrayList<Character> listOfMathOperators) throws Exception {
        double tmp;

        for (int i = 0; i < listOfMathOperators.size(); i++) {
            if (listOfMathOperators.get(i) == Parser.MULTIPLICATION || listOfMathOperators.get(i) == Parser.DIVISION) {
                tmp = calcTwoNumbers(listOfNumbers.get(i), listOfNumbers.get(i + 1), listOfMathOperators.get(i));

                listOfNumbers.set(i, tmp);
                listOfNumbers.remove(i + 1);
                listOfMathOperators.remove(i);
                i--;
            }
        }

        for (int i = 0; i < listOfMathOperators.size(); i++) {
            if (listOfMathOperators.get(i) == Parser.PLUS || listOfMathOperators.get(i) == Parser.MINUS) {
                tmp = calcTwoNumbers(listOfNumbers.get(i), listOfNumbers.get(i + 1), listOfMathOperators.get(i));

                listOfNumbers.set(i, tmp);
                listOfNumbers.remove(i + 1);
                listOfMathOperators.remove(i);
                i--;
            }
        }
        return listOfNumbers.get(0);
    }

    /**
     * Looking for index of first close bracket after last opened
     *
     * @param lastPosOpenIndex last open bracket
     * @param inputString  string with math expression
     * @return pos first open bracket
     */
    public static int findFirstCloseBracket(int lastPosOpenIndex, String inputString) {
        char tmp[] = inputString.toCharArray();
        int firstCloseBracketIndex = 0;
        for (int i = lastPosOpenIndex + 1; i < tmp.length; i++) {
            if (tmp[i] == ')') {
                firstCloseBracketIndex = i;
                break;
            }
        }
        return firstCloseBracketIndex;
    }

    /**
     * Calculates simple math expression
     */
    private static double calcTwoNumbers(double oper1, double oper2, Character sign) throws Exception {
        switch (sign) {
            case Parser.MULTIPLICATION:
                return oper1 * oper2;
            case Parser.DIVISION:
                if (oper2 == 0) {
                    System.out.println("Can't calculate the result, reason - division by zero");
                    System.exit(1);
                } else {
                    return oper1 / oper2;
                }
            case Parser.PLUS:
                return oper1 + oper2;
            case Parser.MINUS:
                return oper1 - oper2;
            default:
                System.out.println("Something went wrong");
                throw new Exception();
        }
    }
}
