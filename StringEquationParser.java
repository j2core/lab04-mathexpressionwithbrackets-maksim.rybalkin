package com.j2core.mrybalkin.week03;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mrybalkin on 5/27/16.
 */
public class StringEquationParser {

    /**
     * Code task 1 week 3
     * Parse simple math expression without brackets, for example 3/4*5+6-7/2-1*4/1.5 and write result
     *
     */
    public static void main(String[] args) {
        String str = "3/4*5+6-7/2-1*4/1.5"; //Hardcoded input string

        mathSolver(parseToNumber(str), parseToMathSign(str));
    }

    /**
     * First - calculate high priority math operations (* and /),
     * remove used numbers and signs from lists.
     * Second - calculate rest of operations (+ and -)
     *
     * @param list              list of numbers (1, 2, etc.)
     * @param listOfMathSigns   list of math signs (+, -, *, /)
     */
    private static void mathSolver(ArrayList<Double> list, ArrayList<Character> listOfMathSigns){
        double tmp;

        for (int i = 0; i < listOfMathSigns.size(); i++) {
            if (listOfMathSigns.get(i).equals('*') || listOfMathSigns.get(i).equals('/')) {
                tmp = calcTwoNumbers(list.get(i), list.get(i + 1), listOfMathSigns.get(i));

                list.set(i, tmp);
                list.remove(i + 1);
                listOfMathSigns.remove(i);
                i--;
            }
        }

        for (int i = 0; i < listOfMathSigns.size(); i++) {
            if (listOfMathSigns.get(i).equals('+') || listOfMathSigns.get(i).equals('-')) {
                tmp = calcTwoNumbers(list.get(i), list.get(i + 1), listOfMathSigns.get(i));

                list.set(i, tmp);
                list.remove(i + 1);
                listOfMathSigns.remove(i);
                i--;
            }
        }

        System.out.println(list.get(0));
    }

    /**
     * Converts string to array of chars,
     * parse element to double,
     * if element is '.' cut string and
     * create double new number as
     *  i-1.i+1
     *
     * @param str - input string
     * @return list of numbers
     */
    private static ArrayList<Double> parseToNumber(String str){
        ArrayList<Double> listOfParsedNumbers = new ArrayList<Double>();
        char[] b = str.toCharArray();
        double tmp;
        for (int i = 0; i < b.length; i++){
            try {
                tmp = Double.parseDouble(String.valueOf(b[i]));
                listOfParsedNumbers.add(tmp);
            } catch (NumberFormatException error){
                if (b[i] == '.') {
                    String strTmp = str.substring(i-1, i+2);
                    tmp = (Double.parseDouble(String.valueOf(strTmp)));

                    listOfParsedNumbers.set(listOfParsedNumbers.size()-1, tmp);
                    i++;
                }
            }
        }

        return listOfParsedNumbers;
    }

    /**
     * Converts string to array of chars,
     * if array element is math sign
     * adds it to collection
     *
     * @param str - input string
     * @return list of math signs
     */
    private static ArrayList<Character> parseToMathSign(String str){
        ArrayList<Character> listOfMathOperators = new ArrayList<Character>();
        char[] b = str.toCharArray();

        for (int i = 0; i < b.length; i++){
            if(isCharacter(str.charAt(i))) {
                listOfMathOperators.add(str.charAt(i));
                }
            }

        return listOfMathOperators;
    }

    /**
     * Calculates simple math expression
     *
     * @param oper1
     * @param oper2
     * @param sign math sign (+, -, *, /)
     * @return result (oper1 sign oper2)
     */
    private static double calcTwoNumbers(double oper1, double oper2, Character sign){
        double result = Double.NaN;

        if (sign.equals('*')) {
            result = oper1 * oper2;
        } else if (sign.equals('/')){
            if (oper2 == 0){
                System.out.println("Division by zero");
            } else {
                result = oper1 / oper2;
            }
        }else if (sign.equals('+')) {
            result = oper1 + oper2;
        }else if (sign.equals('-')){
            result = oper1 - oper2;
        }

        return result;
    }

    static boolean isCharacter(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }
}
