package com.j2core.mrybalkin.week03.console_math_calculator;

/**
 * Created by mrybalkin on 6/17/16.
 */
public class Parser {

    public static final char DIVISION = '/';
    public static final char MULTIPLICATION = '*';
    public static final char PLUS = '+';
    public static final char MINUS = '-';

    /**
     * 1. Verify input string contains alphabetic chars, If yes - exit program
     * 2. Verify input string contains double math signs, e.g. "**" or "..". If yes - replace with one sign
     * 3. Verify input string contains "not assigned" math character
     *
     * E.g. user input string "+1..2+-3**4++5//6", validation will return "1.2-3*4+5/6"
     *
     * @param str - input string
     * @return parsed string
     */
    static String parseInputString(String str){
        StringBuilder sb = new StringBuilder();
        validateBrackets(str);
        String tmp = replacePlusMinus(str);

        for (int i = 0; i < tmp.length(); i++) {
            if (Character.isAlphabetic(tmp.charAt(i))) {
                System.out.println("Expression couldn't be solved, because it contains Alphabetic chars. Please verify for errors and enter again.");
                System.exit(1);
            } else if (tmp.charAt(i) == PLUS || tmp.charAt(i) == MULTIPLICATION ||
                    tmp.charAt(i) == DIVISION || tmp.charAt(i) == '.' || tmp.charAt(i) == MINUS) {
                if (isCharacter(tmp.charAt(i + 1)) && tmp.charAt(i) != tmp.charAt(i + 1)) {
                    System.out.println("Entered expression is invalid. Multitype math sign is entered. Please verify for errors and enter again.");
                    System.exit(1);
                } else if ((tmp.charAt(i) == PLUS || tmp.charAt(i) == MULTIPLICATION || tmp.charAt(i) == DIVISION ) &&
                        (i == 0 || tmp.charAt(i-1) == '(' )){
                    i++;
                }
                while (i < tmp.length() && (tmp.charAt(i) == tmp.charAt(i + 1))) {
                    i++;
                }
                sb.append(tmp.charAt(i));

            } else {
                sb.append(tmp.charAt(i));
            }
        }

        return sb.toString();
    }

    /**
     * Validates input string.
     * If string contains spaces trim it
     * If string contains "+-", "-+" and "--" replace it with "-"
     *
     * @param str - input string
     * @return updated string
     */
    private static String replacePlusMinus(String str){
        return str.replaceAll(" ", "").replaceAll("([+]-|-[+])", "-").replaceAll("--", "-");
    }

    static boolean isCharacter(char c) {
        return c == PLUS || c == MINUS || c == MULTIPLICATION || c == DIVISION;
    }

    /**
     *  Validate input string contains correct number of
     *  open and close brackets
     *
     * @param inputString - string to validate
     */
    static void validateBrackets(String inputString){
        int openBracketCount = 0;
        int closeBracketCount = 0;

        for (Character ch : inputString.toCharArray()){
            if (ch == '('){
                openBracketCount++;
            } else if(ch == ')'){
                closeBracketCount++;
            }
        }

        if (openBracketCount != closeBracketCount){
            System.out.println("You entered not valid expression, please check brackets number and enter again.");
            System.exit(1);
        }

        if (inputString.indexOf(')') < inputString.indexOf('(')){
            System.out.println("You entered not valid expression, please check brackets position and enter again.");
            System.exit(1);
        }
    }
}
